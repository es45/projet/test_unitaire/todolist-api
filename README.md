### Getting started

```bash
docker-compose build --pull --no-cache
docker-compose up -d
```

```bash
docker-compose exec php bin/console d:s:u # set the database
docker-compose exec php bin/console lexik:jwt:generate-keypair # generate jwt keypair
docker-compose exec php ./vendor/bin/phpunit # launch test
```

```
# URL
http://127.0.0.1

# Env DB
DATABASE_URL="postgresql://postgres:password@db:5432/db?serverVersion=13&charset=utf8"
```
