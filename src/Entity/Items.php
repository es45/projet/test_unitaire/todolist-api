<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ItemsRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;


/**
 * @ORM\Entity(repositoryClass=ItemsRepository::class)
 */
#[ApiResource]
class Items
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Type("string")
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Type("string")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Type("DateTime")
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(targetEntity=ToDoList::class, inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private $todolist;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getTodolist(): ?ToDoList
    {
        return $this->todolist;
    }

    public function setTodolist(?ToDoList $todolist): self
    {
        $this->todolist = $todolist;

        return $this;
    }
}
