<?php

namespace App\Controller\Api;

use App\Entity\User;
use Carbon\Carbon;
use JMS\Serializer\Serializer;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\SerializerBuilder;

#[Route('/api')]
class SecurityController extends AbstractController
{
    private Serializer $serializer;
    private UserInterface $user;

    public function __construct()
    {
        $this->serializer = SerializerBuilder::create()->build();
    }
    
    #[Route('/login', name: 'api_login', methods: ['POST'])]
    public function login(Request $request, JWTTokenManagerInterface $JWTManager, UserPasswordHasherInterface $passwordHasher)
    {
        // ...
        $this->user = $this->serializer->deserialize($request->getContent(), User::class, 'json');
        if($this->isValid($this->user)) {
            $this->user->setPassword($passwordHasher->hashPassword($this->user, $this->user->getPassword()));
        }

        return new JsonResponse(['token' => $JWTManager->create($this->user)]);
    }

    #[Route('/register', name: 'api_register', methods: ['POST'])]
    public function register(Request $request, UserPasswordHasherInterface $passwordHasher): JsonResponse
    {
        $user = $this->serializer->deserialize($request->getContent(), User::class, 'json');

        if(empty($user->getEmail()) || empty($user->getPassword())) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }

        if($this->isValid($user)) {
            $user->setPassword($passwordHasher->hashPassword($user, $user->getPassword()));

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }

        return new JsonResponse(['status' => 'Customer created!'], Response::HTTP_CREATED);
    }

    /**
     * @throws \Exception
     */
    public function isValid(User $user): bool
    {
        if (empty($user->getFirstname())) {
            throw new BadRequestHttpException("User does not have a firstname");
        }
        if (empty($user->getLastname())) {
            throw new BadRequestHttpException("User does not have a lastname");
        }
        if (strlen($user->getPassword()) < 8 || strlen($user->getPassword()) > 40) {
            throw new BadRequestHttpException("User password should be between 8 and 40 characters");
        }
        if (!filter_var($user->getEmail(), FILTER_VALIDATE_EMAIL)) {
            throw new BadRequestHttpException("User email invalid");
        }
        if($user->getBirthdate()->diff(Carbon::now())->y <= 12) {
            throw new BadRequestHttpException("User must at least be 13");
        }

        return true;
    }
}
