<?php

namespace App\Controller\Api;

use App\Entity\Items;
use App\Repository\ItemsRepository;
use App\Repository\ToDoListRepository;
use App\Repository\UserRepository;
use App\Service\EmailSenderService;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterval;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api')]
class ItemsController extends AbstractController
{
    #[Route('/items', name: 'items_index', methods: ['GET'])]
    public function index(ItemsRepository $itemsRepository): Response
    {
        return $this->render('items/index.html.twig', [
            'items' => $itemsRepository->findAll(),
        ]);
    }

    #[Route('/items', name: 'items_new', methods: ['POST'])]
    public function new(Request $request, ToDoListRepository $toDoListRepository, UserRepository $userRepository, ItemsRepository $itemsRepository): Response
    {
        $serializer = SerializerBuilder::create()->build();
        $user = $userRepository->findOneBy(["email" => $this->getUser()->getUserIdentifier()]);
        $toDoList = $toDoListRepository->findOneBy(["user_id" => $user->getId()]);

        $data = $request->getContent();
        $item = $serializer->deserialize($data, Items::class, 'json');
        $item->setTodolist($toDoList);
        $item->setCreatedAt(CarbonImmutable::now());
        $existItem = $itemsRepository->findOneBy(["name" => $item->getName(), "todolist" => $toDoList->getId()]);
        $lastItem = $itemsRepository->findOneBy(array("todolist" => $toDoList->getId()), array('id'=>'DESC'));
        $toDoListItems = $toDoList->getItems()->getValues();

        if($this->itemsValidity($item, $existItem, $lastItem, $toDoListItems)) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($item);
            $entityManager->flush();

            if(sizeof($toDoListItems) >= 7) {
                (new EmailSenderService())->sendEmail();
            }
        }

        return new JsonResponse(['status' => 'Items created!'], Response::HTTP_CREATED);
    }

    #[Route('/items/{id}', name: 'items_show', methods: ['GET'])]
    public function show(Items $item): Response
    {
        return $this->render('items/show.html.twig', [
            'item' => $item,
        ]);
    }

    #[Route('/items/{id}', name: 'items_delete', methods: ['POST'])]
    public function delete(Request $request, Items $item): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($item);
        $entityManager->flush();

        return $this->redirectToRoute('items_index', [], Response::HTTP_SEE_OTHER);
    }

    public function itemsValidity(Items $items, $existItem, $lastItem, $toDoListItems)
    {
        if (strlen($items->getContent()) > 1000) {
            throw new BadRequestHttpException("Content must have less than 1000 characters");
        } else if ($existItem) {
            throw new BadRequestHttpException("Name must be unique");
        } else if ($lastItem) {
            if($lastItem->getCreatedAt()->add(CarbonInterval::minutes(30)) > Carbon::now()) {
                throw new BadRequestHttpException("You must wait 30 minutes before create a new item");
            }
        } else if (sizeof($toDoListItems) === 10) {
            throw new BadRequestHttpException("You are limited to 10 items");
        }

        return true;
    }
}
