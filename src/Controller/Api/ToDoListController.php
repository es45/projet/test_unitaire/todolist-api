<?php

namespace App\Controller\Api;

use App\Entity\ToDoList;
use App\Entity\User;
use App\Form\ToDoListType;
use App\Repository\ToDoListRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api')]
class ToDoListController extends AbstractController
{
    #[Route('/todolist', name: 'to_do_list_index', methods: ['GET'])]
    public function index(ToDoListRepository $toDoListRepository): Response
    {
        return new JsonResponse([
            'to_do_lists' => $toDoListRepository->findAll(),
        ], 200);
    }

    #[Route('/todolist', name: 'to_do_list_new', methods: ['POST'])]
    public function new(Request $request, UserRepository $userRepository, ToDoListRepository $toDoListRepository): Response
    {
        $toDoList = new toDoList();
        $data = $userRepository->findOneBy(["email" => $this->getUser()->getUserIdentifier()]);
        $toDoList->setUserId($data);

        $list = $toDoListRepository->findOneBy(["user_id" => $data->getId()]);
        if($list) {
            throw new BadRequestHttpException("User todolist already exist");
        } else {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($toDoList);
            $entityManager->flush();

            return new JsonResponse(['status' => 'TodoList created!'], Response::HTTP_CREATED);
        }
    }

    #[Route('/todolist/{id}', name: 'to_do_list_show', methods: ['GET'])]
    public function show(ToDoList $toDoList): Response
    {
        return new JsonResponse([
            'to_do_list' => $toDoList,
        ], 200);
    }


    #[Route('/todolist/{id}', name: 'to_do_list_delete', methods: ['DELETE'])]
    public function delete(Request $request, ToDoList $toDoList): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($toDoList);
        $entityManager->flush();

        return new JsonResponse(['status' => 'TodoList deleted!'], 200);
    }
}
