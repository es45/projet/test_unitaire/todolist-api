<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Entity\User;
use Carbon\Carbon;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\Uid\Uuid;

abstract class AbstractTest extends ApiTestCase
{
    private ?string $token = null;
    private object $clientWithCredentials;

    private User $user;
    private Serializer $serializer;

    public function setUp(): void
    {
        self::bootKernel();

        $uuid = Uuid::v4();
        $this->serializer = SerializerBuilder::create()->build();
        $this->user = new User();
        $this->user->setEmail($uuid->jsonSerialize() . "test@email.com");
        $this->user->setPassword("testpassword");
        $this->user->setFirstname($uuid->jsonSerialize() . "test");
        $this->user->setLastname($uuid->jsonSerialize() . "test");
        $this->user->setBirthdate(Carbon::now()->subYears(18));
        $this->createUser();
    }

    public function createUser()
    {
        $response = UserControllerTest::createClient()->request('POST', '/api/register', [
            'body' => $this->serializer->serialize($this->user, "json")
        ]);
    }

    protected function createClientWithCredentials($token = null): Client
    {
        $token = $token ?: $this->getToken();

        return static::createClient([], ['headers' => ['authorization' => 'Bearer '.$token]]);
    }

    /**
     * Use other credentials if needed.
     */
    protected function getToken(): string
    {
        if ($this->token) {
            return $this->token;
        }

        $response = static::createClient()->request('POST', '/api/login', [
            'body' => $this->serializer->serialize($this->user, "json")
        ]);

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $this->token = $data->token;

        return $data->token;
    }
}
