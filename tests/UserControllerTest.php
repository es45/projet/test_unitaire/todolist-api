<?php
namespace App\Tests;

use App\Controller\Api\SecurityController;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\User;
use App\Repository\UserRepository;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Uid\Uuid;
use Carbon\Carbon;

final class UserControllerTest extends AbstractTest
{
    private User $user;
    private Serializer $serializer;

    public function setUp(): void
    {
        parent::setUp();

        $uuid = Uuid::v4();
        $this->serializer = SerializerBuilder::create()->build();

        $this->user = new User();
        $this->user->setEmail($uuid->jsonSerialize() . "test@email.com");
        $this->user->setPassword("testpassword");
        $this->user->setFirstname($uuid->jsonSerialize() . "test");
        $this->user->setLastname($uuid->jsonSerialize() . "test");
        $this->user->setBirthdate(Carbon::now()->subYears(18));
    }

    public function testCreateUserEmailIsNotValid() {
        $this->user->setEmail("email.com");
        $response = UserControllerTest::createClient()->request('POST', '/api/register', [
            'body' => $this->serializer->serialize($this->user, "json")
        ]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testCreateUserPasswordIsTooShort() {
        $this->user->setPassword("1234567");
        $response = UserControllerTest::createClient()->request('POST', '/api/register', [
            'body' => $this->serializer->serialize($this->user, "json")
        ]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testCreateUserPasswordIsTooLong() {
        $this->user->setPassword("12345678901234567890123456789012345678901");
        $response = UserControllerTest::createClient()->request('POST', '/api/register', [
            'body' => $this->serializer->serialize($this->user, "json")
        ]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testCreateUserFirstnameIsNotValid() {
        $this->user->setFirstname("");
        $response = UserControllerTest::createClient()->request('POST', '/api/register', [
            'body' => $this->serializer->serialize($this->user, "json")
        ]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testCreateUserLastnameIsNotValid() {
        $this->user->setLastname("");
        $response = UserControllerTest::createClient()->request('POST', '/api/register', [
            'body' => $this->serializer->serialize($this->user, "json")
        ]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testCreateUserBirthdateIsNotValid() {
        $this->user->setBirthdate(Carbon::now()->subYears(10));
        $response = UserControllerTest::createClient()->request('POST', '/api/register', [
            'body' => $this->serializer->serialize($this->user, "json")
        ]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testCreateUser()
    {
        $response = UserControllerTest::createClient()->request('POST', '/api/register', [
            'body' => $this->serializer->serialize($this->user, "json")
        ]);

        $this->assertResponseStatusCodeSame(201);
    }


    public function testUserIsNotLogged()
    {
         $response = UserControllerTest::createClient()->request('GET', '/api/users');

         $this->assertResponseStatusCodeSame(401);
    }

    public function testLoginAsUser()
    {
        $token = $this->getToken();

        $this->createClientWithCredentials($token)->request('GET', '/api/users');
        $this->assertResponseStatusCodeSame(200);
    }


}
