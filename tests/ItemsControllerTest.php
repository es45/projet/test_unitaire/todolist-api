<?php
namespace App\Tests;

use App\Controller\Api\ItemsController;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Controller\Api\SecurityController;
use App\Entity\Items;
use App\Repository\ItemsRepository;
use App\Service\EmailSenderService;
use Carbon\CarbonImmutable;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Uid\Uuid;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

final class ItemsControllerTest extends AbstractTest
{


    private Items $items;
    private Serializer $serializer;
    private $response;

    public function setUp(): void
    {
        parent::setUp();

        $uuid = Uuid::v4();
        $this->serializer = SerializerBuilder::create()->build();

        $this->items = new Items();
        $this->items->setName($uuid->jsonSerialize() . "test");
        $this->items->setContent($uuid->jsonSerialize() . "test");

    }

    public function testCreateToDoList() {
        $response = ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/todolist', []);

        $this->assertResponseStatusCodeSame(201);
    }

    public function testItemContentLengthIsOk() {
        ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/todolist', []);
        $this->assertResponseStatusCodeSame(201);
        $uuid = Uuid::v4();

        $this->items->setName($uuid->jsonSerialize());
        $this->items->setContent("Item content");
        $response = ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/items', [
            'body' => $this->serializer->serialize($this->items, "json")
        ]);

        $this->assertResponseStatusCodeSame(201);
    }

    public function testItemContentLengthIsTooLong() {
        ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/todolist', []);
        $this->assertResponseStatusCodeSame(201);
        $uuid = Uuid::v4();

        $this->items->setName($uuid->jsonSerialize());
        $this->items->setContent("On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L'avantage du Lorem Ipsum sur un texte générique comme 'Du texte. Du texte. Du texte.' est qu'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour 'Lorem Ipsum' vous conduira vers de nombreux sites qui n'en sont encore qu'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d'y rajouter de petits clins d'oeil, voire des phrases embarassantes). On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L'avantage du Lorem Ipsum sur un texte générique comme 'Du texte. Du texte. Du texte.' est qu'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour 'Lorem Ipsum' vous conduira vers de nombreux sites qui n'en sont encore qu'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d'y rajouter de petits clins d'oeil, voire des phrases embarassantes).");

        $response = ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/items', [
            'body' => $this->serializer->serialize($this->items, "json")
        ]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testItemNameAlreadyExists() {
        ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/todolist', []);
        $this->assertResponseStatusCodeSame(201);
        $uuid = Uuid::v4();
        $this->items->setContent("Item content");
        $this->items->setName($uuid->jsonSerialize());

        $response = ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/items', [
            'body' => $this->serializer->serialize($this->items, "json")
        ]);



        // $this->expectException(Exception::class);
        $this->assertResponseStatusCodeSame(400);
        // $this->assertSame("Name must be unique", $response->getContent()->message);
    }

    public function testCreationDelay() {
        ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/todolist', []);
        $this->assertResponseStatusCodeSame(201);

        $this->items->setName(Uuid::v4()->jsonSerialize());
        $this->items->setCreatedAt(CarbonImmutable::now()->addMinutes(90));
        $response = ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/items', [
            'body' => $this->serializer->serialize($this->items, "json")
        ]);
        $this->assertResponseStatusCodeSame(201);

        $this->items->setName(Uuid::v4()->jsonSerialize());
        $this->items->setCreatedAt(CarbonImmutable::now()->addMinutes(92));
        $response = ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/items', [
            'body' => $this->serializer->serialize($this->items, "json")
        ]);
        $this->assertResponseStatusCodeSame(400);

        $this->items->setName(Uuid::v4()->jsonSerialize());
        $this->items->setCreatedAt(CarbonImmutable::now()->addMinutes(140));
        $response = ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/items', [
            'body' => $this->serializer->serialize($this->items, "json")
        ]);
        $this->assertResponseStatusCodeSame(201);
    }

    public function testEmailSended()
    {
        ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/todolist', []);
        $this->assertResponseStatusCodeSame(201);

        $stub = $this->createMock(EmailSenderService::class);

        $stub->method('sendEmail')
            ->willReturn( new JsonResponse(['status' => 'Email Sended!'], Response::HTTP_CREATED));

        for($i = 0; $i < 8; $i++) {
            $this->items->setName(Uuid::v4());
            $this->items->setCreatedAt(CarbonImmutable::now()->addMinutes(30 * $i));

            if( $i === 7 ){
                $this->response = ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/items', [
                    'body' => $this->serializer->serialize($this->items, "json")
                ]);
            } else {
                $rep = ItemsControllerTest::createClientWithCredentials()->request('POST', '/api/items', [
                    'body' => $this->serializer->serialize($this->items, "json")
                ]);
                dump($rep);
            }
        }

        dd($this->response);
        $this->assertResponseStatusCodeSame(201);
        //$this->assertSame('Email Sended!', $stub->sendEmail());
    }
}
